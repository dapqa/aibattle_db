﻿-- Параметры системы
create table system_parameter (
	id			bigserial primary key,
	name		varchar(30) not null unique,
	value_str	varchar(1000) not null,
	description	text
);

-- Язык программирования
create table programming_language (
	id			bigserial primary key,
	name		varchar(100) not null,
	code_type	varchar(15) not null constraint programming_language_run_way check (code_type in ('BINARY', 'BYTE_CODE', 'SCRIPT'))
);

-- Компилятор
create table compiler (
	id							bigserial primary key,
	name						varchar(100) not null,
	storage_path				varchar(200) not null,
	programming_language_id		integer not null references programming_language(id) on delete cascade on update cascade,
	call_string					varchar(1000) not null
);

-- Запускатель (типа JRE, pythonw...)
-- TODO Нужна строка вызова?
create table runner (
	id							bigserial primary key,
	name						varchar(100) not null,
	storage_path				varchar(200) not null,
	programming_language_id		integer not null references programming_language(id) on delete cascade on update cascade
);

-- Системные логи
create table system_log (
	id				bigserial primary key,
	record_date		timestamp not null,
	record_text		text not null,
	account_id		integer references account(id) on delete restrict on update cascade,
	record_type 	varchar(30) not null constraint system_log_record_type check
		(record_type in ('INFO', 'ERROR')),
	record_section	varchar(30) not null constraint system_log_record_section check
		(record_section in ('SYSTEM', 'TOURNAMENT_SCHEME', 'ORGANIZER', 'LOGGER', 'BOT', 'TOURNAMENT'))
);

-- Логгер игр
create table logger (
	id 							bigserial primary key,
	name						varchar(100) not null,
	programming_language_id		integer not null references programming_language(id) on delete restrict on update cascade,
	storage_path				varchar(200) not null,
	state_name					varchar(30) not null constraint logger_state_name check 
		(state_name in ('SENT', 'COMPILATION_OK', 'COMPILATION_ERR', 'INTERFACE_ERR', 'TEST_FAIL', 'ACCEPTED'))	
);

-- Организатор
create table organizer (
	id 							bigserial primary key,
	name						varchar(100) not null,
	programming_language_id		integer not null references programming_language(id) on delete restrict on update cascade,
	storage_path				varchar(200) not null,
	player_count				smallint not null,
	state_name					varchar(30) not null constraint organizer_state_name check 
		(state_name in ('SENT', 'COMPILATION_OK', 'COMPILATION_ERR', 'INTERFACE_OK', 'INTERFACE_ERR', 'TEST_GAME_ERR', 'ACCEPTED'))
);

-- Схема турнира
create table tournament_scheme (
	id 							bigserial primary key,
	name						varchar(100) not null,
	programming_language_id		integer not null references programming_language(id) on delete restrict on update cascade,
	storage_path				varchar(200) not null,
	state_name					varchar(10) not null constraint tournament_scheme_state_name check 
		(state_name in ('SENT', 'COMPILATION_OK', 'COMPILATION_ERR', 'INTERFACE_OK', 'INTERFACE_ERR', 'TEST_FAIL', 'ACCEPTED'))
);

-- Турнир
create table tournament (
	id 							bigserial primary key,
	name						varchar(100) not null,
	description_file_path		varchar(200) not null,
	is_closed					boolean not null default false,
	is_admin_private			boolean not null default false,
	is_hidden					boolean not null default false,
	creator_id					integer not null references account(id) on delete restrict on update cascade
		constraint tournament_creator_access_level check (is_admin(creator_id)),
	logger_id					integer not null references logger(id) on delete restrict on update cascade,
	organizer_id				integer not null references organizer(id) on delete restrict on update cascade,
	tournament_scheme_id		integer not null references tournament_scheme(id) on delete restrict on update cascade
);

-- Параметры турниров
create table tournament_parameter (
	id				bigserial primary key,
	tournament_id	integer not null references tournament(id) on delete cascade on update cascade,
	name			varchar(30) not null,
	value_str		varchar(1000) not null,
	description		text
);

-- Визуализатор
create table visualizer (
	id 					bigserial primary key,
	name				varchar(100) not null,
	storage_path		varchar(200) not null,
	tournament_id		integer references tournament(id) on delete restrict on update cascade,
	description 		text
);

-- Доступ к участию в турнире
create table tournament_participant_access (
	id				bigserial primary key,
	tournament_id	integer references tournament(id) on delete cascade on update cascade,
	account_id		integer references account(id) on delete cascade on update cascade
		constraint tournament_participant_access_level check (is_participant(account_id)),
	state_name		varchar(30) not null constraint tournament_participant_access_state_name check
		(state_name in ('IN_PROCESS', 'ACCEPTED', 'REJECTED'))
);

-- Доступ к запуску турнира
create table tournament_run_access (
	id				bigserial primary key,
	tournament_id	integer references tournament(id) on delete cascade on update cascade,
	account_id		integer references account(id) on delete cascade on update cascade
		constraint tournament_run_access_level check (is_admin(account_id))
);

-- Бот
create table bot (
	id				bigserial primary key,
	tournament_id	integer references tournament(id) on delete cascade on update cascade,
	is_test			boolean,
	storage_path    varchar(200) not null,
	state_name		varchar(30) not null constraint organizer_state_name check 
		(state_name in ('SENT', 'COMPILATION_OK', 'COMPILATION_ERR', 'INTERFACE_OK', 'INTERFACE_ERR', 'TEST_GAME_ERR', 'ACCEPTED', 'REPLACED', 'LULLED')),
	author_id		integer references account(id) on delete cascade on update cascade
);

-- Запуски турниров
create table tournament_run (
	id				bigserial primary key,
	tournament_id	integer references tournament(id) on delete cascade on update cascade,
	run_date		timestamp not null,
	admin_id		integer references account(id) on delete restrict on update cascade
		constraint tournament_run_admin_access_level check (is_admin(admin_id)),
	is_finished		boolean not null default true
);

-- Одна игра в процессе турнира
create table game (
	id					bigserial primary key,
	tournament_run_id	integer references tournament_run(id) on delete cascade on update cascade,
	is_finished			boolean not null default true,
	log_name			varchar(100),
	additional_info		text
);

-- Результат участия одного бота в игре
create table game_participant (
	id			bigserial primary key,
	game_id		integer references game(id) on delete cascade on update cascade,
	bot_id		integer references bot(id) on delete restrict on update cascade,
	score		integer not null
);

-- Результаты участия бота в одном запуске турнире
create table bot_tournament_result (
	id					bigserial primary key,
	tournament_run_id	integer references tournament_run(id) on delete cascade on update cascade,
	bot_id				integer references bot(id) on delete restrict on update cascade,
	score				integer not null
);

-- Новости системы
create table global_message (
	id			bigserial primary key,
	author_id	integer references account(id) on delete cascade on update cascade
		constraint global_message_access_level check (is_admin(author_id)),
	content		text not null,
	create_date	timestamp not null
);

-- Новости турнира
create table tournament_message (
	id				bigserial primary key,
	tournament_id	integer references tournament(id) on delete cascade on update cascade,
	author_id		integer references account(id) on delete cascade on update cascade
		constraint global_message_access_level check (is_admin(author_id)),
	content			text not null,
	create_date		timestamp not null
);