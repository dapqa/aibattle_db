﻿-- Пользователь
create table account (
	id				bigserial primary key,
	login			varchar(30) not null unique,
	password_hash	char(32) not null,
	access_level	varchar(15) not null constraint account_access_level check (access_level in ('ADMINISTRATOR', 'PARTICIPANT')),
	first_name		varchar(30) not null,
	second_name		varchar(30) not null,
	middle_name		varchar(30),
	gender			char(1) check (gender in ('M', 'F')),
	email			varchar(100),
	info			text,
	is_banned		boolean not null default false
);

-- Проверка прав (админ)
create function is_admin (
	pAccountId	integer
) returns boolean as $$
declare
	r	account%rowtype;
begin
	select * into r from account where account.id = pAccountId;
	if r.is_banned then raise exception 'Пользователь забанен';
	elsif r.access_level != 'ADMINISTRATOR' then raise exception 'Действие может совершить только администратор';
	end if;
	return true;
end;
$$ language plpgsql;

-- Проверка прав (участник)
create function is_participant (
	pAccountId	integer
) returns boolean as $$
declare
	r	account%rowtype;
begin
	select * into r from account where account.id = pAccountId;
	if r.is_banned then raise exception 'Пользователь забанен';
	elsif r.access_level != 'PARTICIPANT' then raise exception 'Действие может совершить только пользователь с правами участника';
	end if;
	return true;
end;
$$ language plpgsql;