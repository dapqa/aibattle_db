﻿drop view if exists account_v cascade;
create or replace view account_v as select * from account;

drop view if exists system_parameter_v cascade;
create or replace view system_parameter_v as select * from system_parameter;

drop view if exists programming_language_v cascade;
create or replace view programming_language_v as select * from programming_language;

drop view if exists compiler_v cascade;
create or replace view compiler_v as 
select
	c.id,
	c.name,
	c.storage_path,
	c.call_string,
	c.programming_language_id,
	pl.name as programming_language_name,
	pl.code_type as programming_language_code_type
from compiler c, programming_language pl
where pl.id = c.programming_language_id;

drop view if exists runner_v cascade;
create or replace view runner_v as
select
	r.id,
	r.name,
	r.storage_path,
	r.programming_language_id,
	pl.name as programming_language_name,
	pl.code_type as programming_language_code_type
from runner r, programming_language pl
where pl.id = r.programming_language_id;

drop view if exists system_log_v cascade;
create or replace view system_log_v as 
select
	sl.id,
	sl.record_date,
	sl.record_text,
	sl.record_type,
	sl.record_section,
	sl.account_id,
	a.login as account_login,
	a.first_name as account_first_name,
	a.second_name as account_second_name
from system_log as sl
left outer join account as a on sl.account_id = a.id;

drop view if exists logger_v cascade;
create or replace view logger_v as 
select
	l.id,
	l.name,
	l.storage_path,
	l.state_name,
	l.programming_language_id,
	pl.name as programming_language_name,
	pl.code_type as programming_language_code_type
from logger l, programming_language pl
where pl.id = l.programming_language_id;

drop view if exists organizer_v cascade;
create or replace view organizer_v as 
select
	o.id,
	o.name,
	o.storage_path,
	o.state_name,
	o.programming_language_id,
	o.player_count,
	pl.name as programming_language_name,
	pl.code_type as programming_language_code_type
from organizer o, programming_language pl
where pl.id = o.programming_language_id;

drop view if exists tournament_scheme_v cascade;
create or replace view tournament_scheme_v as
select
	ts.id,
	ts.name,
	ts.storage_path,
	ts.state_name,
	ts.programming_language_id,
	pl.name as programming_language_name,
	pl.code_type as programming_language_code_type
from tournament_scheme ts, programming_language pl
where pl.id = ts.programming_language_id;

-- Для веб-интерфейса
drop view if exists tournament_wiv cascade;
create or replace view tournament_wiv as
select
	t.id,
	t.name,
	t.description_file_path,
	t.creator_id,
	t.is_closed,
	t.is_admin_private,
	t.is_hidden,
	a.login as creator_login,
	a.first_name as creator_first_name,
	a.second_name as creator_second_name,
	t.logger_id,
	l.name as logger_name,
	t.organizer_id,
	o.name as organizer_name,
	o.programming_language_name as organizer_programming_language_name,
	t.tournament_scheme_id,
	ts.name as tournament_scheme_name,
	ts.programming_language_name as tournament_scheme_programming_language_name
from tournament t, account a, logger_v l, organizer_v o, tournament_scheme_v ts
where
	t.creator_id = a.id and
	t.logger_id = l.id and
	t.organizer_id = o.id and
	t.tournament_scheme_id = ts.id;
	
-- Для запуска турниров системой
drop view if exists tournament_tv cascade;
create or replace view tournament_tv as
select
	t.id,
	t.organizer_id,
	l.programming_language_id as logger_programming_lanuage_id,
	l.programming_language_code_type as logger_programming_lanuage_code_type,
	l.storage_path,
	o.programming_language_id as organizer_programming_language_id,
	o.programming_language_code_type as organizer_programming_language_code_type,
	o.storage_path as organizer_storage_path,
	o.player_count as organizer_player_count,
	t.tournament_scheme_id,
	ts.programming_language_id as tournament_scheme_programming_language_id,
	ts.programming_language_code_type as tournament_scheme_programming_language_code_type,
	ts.storage_path as tournament_scheme_storage_path
from tournament t, logger_v l, organizer_v o, tournament_scheme_v ts
where
	t.logger_id = l.id and
	t.organizer_id = o.id and
	t.tournament_scheme_id = ts.id;

drop view if exists tournament_parameter_v cascade;
create or replace view tournament_parameter_v as select * from tournament_parameter;

drop view if exists visualizer_v cascade;
create or replace view visualizer_v as select * from visualizer;

-- Для администраторов, чтобы принимать/отклонять заявки,
-- а также для просмотра статуса заявок
drop view if exists tournament_participant_access_v cascade;
create or replace view tournament_participant_access_v as
select
	tpa.id,
	tpa.tournament_id,
	tpa.state_name,
	tpa.account_id,
	a.login as account_login,
	a.first_name as account_first_name,
	a.second_name as account_second_name,
	a.info as account_info
from tournament_participant_access tpa, account a
where tpa.account_id = a.id;	

-- Для быстрой фильтрации списка турниров при показе
drop view if exists tournament_participant_access_av cascade;
create or replace view tournament_participant_access_av as
select
	tpa.id,
	tpa.tournament_id,
	tpa.account_id
from tournament_participant_access tpa
where tpa.state_name = 'ACCEPTED';

drop view if exists tournament_run_access_v cascade;
create or replace view tournament_run_access_v as
select
	tra.id,
	tra.tournament_id,
	tra.account_id,
	a.login as account_login,
	a.first_name as account_first_name,
	a.second_name as account_second_name,
	a.info as account_info
from tournament_run_access tra, account a
where tra.account_id = a.id;

drop view if exists bot_v cascade;
create or replace view bot_v as 
select
	b.id,
	b.tournament_id,
	b.is_test,
	b.state_name,
	b.author_id,
	a.login as author_login,
	a.first_name as author_first_name,
	a.second_name as author_second_name
from bot b, account a
where b.author_id = a.id;

drop view if exists tournament_run_v cascade;
create or replace view tournament_run_v as
select
	tr.id,
	tr.run_date,
	tr.is_finished,
	tr.tournament_id,
	t.name as tournament_name,
	tr.admin_id,
	a.login as admin_login,
	a.first_name as admin_first_name,
	a.second_name as admin_second_name
from tournament_run tr, tournament t, account a
where tr.tournament_id = t.id and tr.admin_id = a.id;

-- TODO Генерить список участников игры в базе?
drop view if exists game_v cascade;
create or replace view game_v as select * from game;
	
drop view if exists game_participant_v cascade;
create or replace view game_participant_v as 
select
	gp.id,
	gp.game_id,
	gp.score,
	gp.bot_id,
	b.author_login as bot_author_login,
	b.author_first_name as bot_author_first_name,
	b.author_second_name as bot_author_second_name
from game_participant gp, bot_v b
where gp.bot_id = b.id;

drop view if exists bot_tournament_result_v cascade;
create or replace view bot_tournament_result_v as
select
	btr.id,
	btr.score,
	btr.tournament_run_id,
	tr.tournament_name as tournament_run_tournament_name,
	btr.bot_id,
	b.author_login as bot_author_login,
	b.author_first_name as bot_author_first_name,
	b.author_second_name as bot_author_second_name
from bot_tournament_result btr, tournament_run_v tr, bot_v b
where
	btr.tournament_run_id = tr.id and
	btr.bot_id = b.id;
	
drop view if exists global_message_v cascade;
create or replace view global_message_v as
select
	gm.id,
	gm.content,
	gm.create_date,
	gm.author_id,
	a.login as author_login,
	a.first_name as author_first_name,
	a.second_name as author_second_name
from global_message gm, account a
where gm.author_id = a.id;

drop view if exists tournament_message_v cascade;
create or replace view tournament_message_v as
select
	tm.id,
	tm.tournament_id,
	tm.content,
	tm.create_date,
	tm.author_id,
	a.login as author_login,
	a.first_name as author_first_name,
	a.second_name as author_second_name
from tournament_message tm, account a
where tm.author_id = a.id;