package ai.battle.test;

import ai.battle.db.dao.CommonDAO;
import ai.battle.db.dto.base.BaseDTO;
import ai.battle.db.dto.system.AccountDTO;
import ai.battle.db.dto.system.ProgrammingLanguageDTO;
import ai.battle.db.dto.system.SystemLogDTO;
import ai.battle.db.dto.system.SystemParameterDTO;
import ai.battle.db.dto.tournament.LoggerDTO;
import ai.battle.db.dto.tournament.OrganizerDTO;
import ai.battle.db.dto.tournament.TournamentDTO;
import ai.battle.db.dto.tournament.TournamentSchemeDTO;
import ai.battle.db.sc.BaseNameSC;
import ai.battle.db.sc.BaseSC;
import ai.battle.db.sc.SortingDescription;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.*;

/**
 * Created by dapqa on 19.03.2016.
 */
@SuppressWarnings("unchecked")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:META-INF/beans.xml" })
public class DBTest {

    @Autowired
    private CommonDAO commonDAO;

    private MockFactory mockFactory = new MockFactory();

    @Test
    public void testTournamentCRUD() {
        // Создание сущностей для частей турнира
        AccountDTO accountDTOMock = mockFactory.createAccountDTOMock();
        assertInsert(accountDTOMock);

        ProgrammingLanguageDTO programmingLanguageDTOMock = mockFactory.createProgrammingLanguageDTOMock();
        assertInsert(programmingLanguageDTOMock);

        OrganizerDTO organizerDTOMock = mockFactory.createOrganizerDTOMock(programmingLanguageDTOMock);
        assertInsert(organizerDTOMock);

        LoggerDTO loggerDTOMock = mockFactory.createLoggerDTOMock(programmingLanguageDTOMock);
        assertInsert(loggerDTOMock);

        TournamentSchemeDTO tournamentSchemeDTOMock = mockFactory
                .createTournamentSchemeDTOMock(programmingLanguageDTOMock);
        assertInsert(tournamentSchemeDTOMock);

        // Создание сущности турнира
        TournamentDTO tournamentDTOMock = mockFactory.createTournamentDTOMock(
                accountDTOMock, organizerDTOMock, loggerDTOMock, tournamentSchemeDTOMock
        );
        Long tournamentId = assertInsert(tournamentDTOMock);

        // Чтение сущености турнира
        TournamentDTO tournamentDTO = commonDAO.get(TournamentDTO.class, createBaseSC(tournamentId)).get(0);
        assertTrue(tournamentDTO.equals(tournamentDTOMock));

        // Изменение сущности турнира
        tournamentDTOMock.setName("Изменено");
        Long changedTournamentDTOId = commonDAO.post(tournamentDTOMock);
        assertEquals(changedTournamentDTOId, tournamentId);

        tournamentDTO = commonDAO.getById(TournamentDTO.class, tournamentId);
        assertTrue(tournamentDTO.equals(tournamentDTOMock));

        // Удаление всех созданных сущностей
        assertDelete(tournamentDTOMock);
        assertDelete(loggerDTOMock);
        assertDelete(organizerDTOMock);
        assertDelete(tournamentSchemeDTOMock);
        assertDelete(accountDTOMock);
        assertDelete(programmingLanguageDTOMock);
    }

    @Test
    public void testNotNullField() {
        AccountDTO accountDTOMock = mockFactory.createAccountDTOMock();
        accountDTOMock.setLogin(null);
        try {
            commonDAO.post(accountDTOMock);
            fail();
        } catch (DataIntegrityViolationException e) {
            assertTrue(e.getMessage().contains("\"login\" нарушает ограничение NOT NULL"));
        }
    }

    @Test
    public void testVarCharLengthOverflow() {
        AccountDTO accountDTOMock = mockFactory.createAccountDTOMock();
        accountDTOMock.setLogin("a_a_a_a_a_a_a_a_a_a_a_a_a_a_a_a_a_a_a_a_a_a_a_a_a_a_a_a_a_a_a_a_a_a_a_a_a_a_a");
        try {
            commonDAO.post(accountDTOMock);
            fail();
        } catch (DataIntegrityViolationException e) {
            assertTrue(e.getMessage().contains("ОШИБКА: значение не умещается в тип character varying"));
        }
    }

    @Test
    public void testInvalidForeignKey() {
        SystemLogDTO systemLogDTOMock = mockFactory.createSystemLogDTOMock(new AccountDTO());

        Random random = new Random();
        Long id = 100_000_000L;
        while (commonDAO.getCount(AccountDTO.class, createBaseSC(id)) != 0L) {
            id += random.nextLong() % 100_000_000L + 1L;
        }

        systemLogDTOMock.getAccount().setId(id);
        try {
            commonDAO.post(systemLogDTOMock);
            fail();
        } catch (DataIntegrityViolationException e) {
            assertTrue(e.getMessage().contains("ОШИБКА: INSERT или UPDATE в таблице \"system_log\" нарушает " +
                    "ограничение внешнего ключа \"system_log_account_id_fkey\""));
        }
    }

    @Test
    public void testDeleteRestrictConstraint() {
        ProgrammingLanguageDTO programmingLanguageDTOMock = mockFactory.createProgrammingLanguageDTOMock();
        assertInsert(programmingLanguageDTOMock);

        OrganizerDTO organizerDTOMock = mockFactory.createOrganizerDTOMock(programmingLanguageDTOMock);
        assertInsert(organizerDTOMock);

        try {
            commonDAO.delete(programmingLanguageDTOMock);
            // Сюда прийти не должно
            commonDAO.delete(organizerDTOMock);
            fail();
        } catch (Exception e) {
            assertDelete(organizerDTOMock);
            assertDelete(programmingLanguageDTOMock);
            assertTrue(e.getMessage().contains("всё ещё есть ссылки в таблице \"organizer\""));
        }
    }

    @Test
    public void testDates() {
        SystemLogDTO systemLogDTOMock = mockFactory.createSystemLogDTOMock(new AccountDTO());
        Long id = assertInsert(systemLogDTOMock);

        SystemLogDTO systemLogDTO = commonDAO.getById(SystemLogDTO.class, id);
        assertTrue(systemLogDTO.equals(systemLogDTOMock));

        assertDelete(systemLogDTOMock);
    }

    @Test
    public void testSortingAndLimitAndOffset() {
        List<SystemParameterDTO> mocks = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            SystemParameterDTO systemParameterDTOMock = mockFactory.createSystemParameterDTOMock();
            String name = systemParameterDTOMock.getName();
            systemParameterDTOMock.setName(name + Integer.toString(i));

            assertInsert(systemParameterDTOMock);
            mocks.add(systemParameterDTOMock);
        }

        BaseSC sc = new BaseSC();
        sc.addSorting("id", SortingDescription.Sorting.DESC);
        sc.setLimit(5L);
        sc.setOffset(2L);
        List<SystemParameterDTO> systemParameterDTOList = commonDAO.get(SystemParameterDTO.class, sc);

        mocks.forEach(this::assertDelete);

        assertEquals(5, systemParameterDTOList.size());
        assertEquals(mocks.get(7).getId(), systemParameterDTOList.get(0).getId());
        for (int i = 1; i < 5; i++) {
            assertTrue(systemParameterDTOList.get(i - 1).getId() > systemParameterDTOList.get(i).getId());
        }
    }

    @Test
    public void testILike() {
        BaseNameSC baseNameSC = new BaseNameSC();
        baseNameSC.setName("nAm");
        long initCount = commonDAO.getCount(SystemParameterDTO.class, baseNameSC);

        SystemParameterDTO systemParameterDTOMock = mockFactory.createSystemParameterDTOMock();
        systemParameterDTOMock.setName("TestName");
        assertInsert(systemParameterDTOMock);

        long count = commonDAO.getCount(SystemParameterDTO.class, baseNameSC);

        assertDelete(systemParameterDTOMock);
        assertEquals(1L, count - initCount);
    }

    private BaseSC createBaseSC(Long id) {
        BaseSC res = new BaseSC();
        res.setId(id);
        return res;
    }

    private <T extends BaseDTO> Long assertInsert(T dtoMock) {
        Long id = commonDAO.post(dtoMock);
        assertNotNull(id);
        dtoMock.setId(id);
        return id;
    }

    private <T extends BaseDTO> void assertDelete(T dtoMock) {
        Long id = dtoMock.getId();
        commonDAO.delete(dtoMock);
        assertEquals(0L, commonDAO.getCount(dtoMock.getClass(), createBaseSC(id)));
    }

}
