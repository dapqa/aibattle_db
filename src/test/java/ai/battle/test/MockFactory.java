package ai.battle.test;

import ai.battle.db.dto.system.AccountDTO;
import ai.battle.db.dto.system.ProgrammingLanguageDTO;
import ai.battle.db.dto.system.SystemLogDTO;
import ai.battle.db.dto.system.SystemParameterDTO;
import ai.battle.db.dto.tournament.LoggerDTO;
import ai.battle.db.dto.tournament.OrganizerDTO;
import ai.battle.db.dto.tournament.TournamentDTO;
import ai.battle.db.dto.tournament.TournamentSchemeDTO;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by dapqa on 21.03.2016.
 */
public class MockFactory {

    private Date testDate;

    public MockFactory() {
        try {
            testDate = new SimpleDateFormat("yyyy-MM-dd").parse("2016-03-20");
        } catch (ParseException ignore) {}
    }

    public AccountDTO createAccountDTOMock() {
        AccountDTO accountDTOMock = new AccountDTO();

        accountDTOMock.setAccessLevel(AccountDTO.AccessLevel.ADMINISTRATOR);
        accountDTOMock.setBanned(Boolean.FALSE);
        accountDTOMock.setLogin("TEST____TEST");
        accountDTOMock.setPasswordHash("202cb962ac59075b964b07152d234b70");
        accountDTOMock.setEmail("test@db.ru");
        accountDTOMock.setFirstName("Арсений");
        accountDTOMock.setSecondName("Пуля");
        accountDTOMock.setGender(AccountDTO.Gender.M);

        return accountDTOMock;
    }

    public ProgrammingLanguageDTO createProgrammingLanguageDTOMock() {
        ProgrammingLanguageDTO programmingLanguageDTOMock = new ProgrammingLanguageDTO();

        programmingLanguageDTOMock.setName("C++");
        programmingLanguageDTOMock.setCodeType(ProgrammingLanguageDTO.CodeType.BINARY);

        return programmingLanguageDTOMock;
    }

    public OrganizerDTO createOrganizerDTOMock(ProgrammingLanguageDTO programmingLanguage) {
        OrganizerDTO organizerDTOMock = new OrganizerDTO();

        organizerDTOMock.setProgrammingLanguage(programmingLanguage);
        organizerDTOMock.setName("TEST_ORGANIZER");
        organizerDTOMock.setStoragePath("aaa/bbb");
        organizerDTOMock.setPlayerCount(2);
        organizerDTOMock.setState(OrganizerDTO.State.ACCEPTED);

        return organizerDTOMock;
    }

    public LoggerDTO createLoggerDTOMock(ProgrammingLanguageDTO programmingLanguage) {
        LoggerDTO loggerDTOMock = new LoggerDTO();

        loggerDTOMock.setStoragePath("ccc/ddd");
        loggerDTOMock.setName("TEST_LOGGER");
        loggerDTOMock.setState(LoggerDTO.State.ACCEPTED);
        loggerDTOMock.setProgrammingLanguage(programmingLanguage);

        return loggerDTOMock;
    }

    public TournamentSchemeDTO createTournamentSchemeDTOMock(ProgrammingLanguageDTO programmingLanguage) {
        TournamentSchemeDTO tournamentSchemeDTOMock = new TournamentSchemeDTO();

        tournamentSchemeDTOMock.setProgrammingLanguage(programmingLanguage);
        tournamentSchemeDTOMock.setState(TournamentSchemeDTO.State.ACCEPTED);
        tournamentSchemeDTOMock.setName("TEST_TS");
        tournamentSchemeDTOMock.setStoragePath("eee/fff");

        return tournamentSchemeDTOMock;
    }

    public TournamentDTO createTournamentDTOMock(
            AccountDTO creator,
            OrganizerDTO organizer,
            LoggerDTO logger,
            TournamentSchemeDTO tournamentScheme
    ) {
        TournamentDTO tournamentDTOMock = new TournamentDTO();

        tournamentDTOMock.setName("TEST_TOURNAMENT");
        tournamentDTOMock.setAdminPrivate(true);
        tournamentDTOMock.setClosed(false);
        tournamentDTOMock.setDescriptionFilePath("ggg/hhh");
        tournamentDTOMock.setHidden(false);
        tournamentDTOMock.setCreator(creator);
        tournamentDTOMock.setLogger(logger);
        tournamentDTOMock.setOrganizer(organizer);
        tournamentDTOMock.setTournamentScheme(tournamentScheme);

        return tournamentDTOMock;
    }

    public SystemLogDTO createSystemLogDTOMock(AccountDTO account) {
        SystemLogDTO systemLogDTOMock = new SystemLogDTO();

        systemLogDTOMock.setAccount(account);
        systemLogDTOMock.setRecordDate(testDate);
        systemLogDTOMock.setRecordText("Тестовая запись лога");
        systemLogDTOMock.setRecordSection(SystemLogDTO.RecordSection.SYSTEM);
        systemLogDTOMock.setRecordType(SystemLogDTO.RecordType.INFO);

        return systemLogDTOMock;
    }

    public SystemParameterDTO createSystemParameterDTOMock() {
        SystemParameterDTO systemParameterDTOMock = new SystemParameterDTO();

        systemParameterDTOMock.setDescription("Тестовое описание");
        systemParameterDTOMock.setValueStr("Тестовое значение");
        systemParameterDTOMock.setName("TEST_PARAM");

        return systemParameterDTOMock;
    }

}
