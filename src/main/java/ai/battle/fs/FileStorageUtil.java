package ai.battle.fs;

import ai.battle.db.dto.base.BaseDTO;
import ai.battle.db.dto.base.NamedRunnableDTO;
import ai.battle.db.dto.tournament.BotDTO;
import ai.battle.db.dto.tournament.GameDTO;
import ai.battle.db.dto.tournament.VisualizerDTO;
import ai.battle.db.util.property.NamingUtil;
import org.springframework.stereotype.Component;

import java.io.File;

@Component
public class FileStorageUtil {

    // TODO: Заглушка, необходимо получать путь из БД
    private String storagePath = "C:/AIBattleStorage";

    public <T extends BaseDTO> String getPath(T obj) {
        String id = obj.getId().toString();
        String relativePath = null;
        String dirName = null;

        if (obj instanceof NamedRunnableDTO) {
            relativePath = ((NamedRunnableDTO) obj).getStoragePath();
            dirName = NamingUtil.tableNameFromDTOClassName(obj.getClass().getSimpleName());
        } else if (obj instanceof BotDTO) {
            relativePath = ((BotDTO) obj).getStoragePath();
            dirName = "bot";
        } else if (obj instanceof VisualizerDTO) {
            relativePath = ((VisualizerDTO) obj).getStoragePath();
            dirName = "visualizer";
        } else if (obj instanceof GameDTO) {
            relativePath = ((GameDTO) obj).getLogName();
            dirName = "log";
        }

        if (relativePath != null) {
            return String.format("%s/%s/%s/%s", storagePath, dirName, id, relativePath);
        } else {
            return null;
        }
    }

    public <T extends BaseDTO> File getFile(T obj) {
        String path = getPath(obj);
        if (path != null) {
            return new File(path);
        } else {
            return null;
        }
    }
}
