package ai.battle.db.dao;

import ai.battle.db.dto.base.BaseDTO;
import ai.battle.db.sc.BaseSC;
import ai.battle.db.util.AIBattleJdbcTemplate;
import ai.battle.db.util.QueryBuilder;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.annotation.Resource;
import javax.sql.DataSource;

/**
 * Created by dapqa on 09.03.2016.
 */
public class BaseDAO {

    protected AIBattleJdbcTemplate jdbcTemplate;

    @Resource
    public final void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new AIBattleJdbcTemplate(dataSource);
    }

    protected <T extends BaseDTO> boolean isNew(T dto) {
        return dto.getId() == null;
    }

    protected <T extends BaseSC> void fillBaseSC(QueryBuilder builder, T sc) {
        builder.eq("id", sc.getId());
    }

}
