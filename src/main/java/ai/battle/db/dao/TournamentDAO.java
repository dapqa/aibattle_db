package ai.battle.db.dao;

import ai.battle.db.dto.tournament.TournamentDTO;
import ai.battle.db.sc.BaseSC;

import java.util.List;

/**
 * Created by dapqa on 09.03.2016.
 */
public interface TournamentDAO {

    List<TournamentDTO> getTournamentsFromTournamentView(BaseSC sc);
}
