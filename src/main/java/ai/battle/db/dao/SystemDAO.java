package ai.battle.db.dao;

import ai.battle.db.dto.system.TournamentParticipantAccessDTO;
import ai.battle.db.sc.BaseSC;

import java.util.List;

/**
 * Created by dapqa on 09.03.2016.
 */
public interface SystemDAO {

    List<TournamentParticipantAccessDTO> getOnlyAcceptedTournamentParticipantAccesses(BaseSC sc);

}
