package ai.battle.db.dao;

import ai.battle.db.dto.base.BaseDTO;
import ai.battle.db.sc.BaseSC;

import java.util.List;

/**
 * Created by dapqa on 20.03.2016.
 */
public interface CommonDAO {

    <T extends BaseDTO, SC extends BaseSC> List<T> get(Class<T> dtoClass, SC sc);

    <T extends BaseDTO, SC extends BaseSC> long getCount(Class<T> dtoClass, SC sc);

    <T extends BaseDTO> T getById(Class<T> dtoClass, long id);

    <T extends BaseDTO> Long post(T object);

    <T extends BaseDTO> void delete(T object);

    <T extends BaseDTO> void deleteById(Class<T> dtoClass, long id);

}
