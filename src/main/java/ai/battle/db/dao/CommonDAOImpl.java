package ai.battle.db.dao;

import ai.battle.db.dto.base.BaseDTO;
import ai.battle.db.dto.system.*;
import ai.battle.db.dto.tournament.*;
import ai.battle.db.sc.BaseSC;
import ai.battle.db.util.QueryBuilder;
import ai.battle.db.util.property.NamingUtil;
import ai.battle.db.util.property.PropertyPoster;
import ai.battle.db.util.property.PropertyRowMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;

/**
 * Created by dapqa on 20.03.2016.
 */
@Service("commonDAO")
@SuppressWarnings("unchecked")
public class CommonDAOImpl extends BaseDAO implements CommonDAO {

    private static final Class[] DTO_CLASSES = new Class[] {
            AccountDTO.class, CompilerDTO.class, GlobalMessageDTO.class,
            ProgrammingLanguageDTO.class, RunnerDTO.class, SystemLogDTO.class,
            SystemParameterDTO.class, TournamentMessageDTO.class, TournamentParticipantAccessDTO.class,
            TournamentRunAccessDTO.class, BotDTO.class, BotTournamentResultDTO.class,
            GameDTO.class, GameParticipantDTO.class, LoggerDTO.class,
            OrganizerDTO.class, TournamentDTO.class, TournamentParameterDTO.class,
            TournamentRunDTO.class, TournamentSchemeDTO.class, VisualizerDTO.class
    };

    protected HashMap<String, PropertyRowMapper> propertyRowMappers = new HashMap<>();
    protected HashMap<String, PropertyPoster> propertyPosters = new HashMap<>();
    protected HashMap<String, String> viewsNames = new HashMap<>();

    @PostConstruct
    @SuppressWarnings("unused")
    private void init() {
        for (Class dtoClass : DTO_CLASSES) {
            String key = dtoClass.getCanonicalName();
            String tableName = NamingUtil.tableNameFromDTOClassName(dtoClass.getSimpleName());

            propertyRowMappers.put(key, new PropertyRowMapper(dtoClass));
            propertyPosters.put(key, new PropertyPoster(dtoClass));
            viewsNames.put(key, tableName + "_v");
        }

        // Здесь следует добавить hint'ы для PropertyRowMapper'ов, а также
        // заменить имена вьюх по необходимости
        viewsNames.put(TournamentDTO.class.getCanonicalName(), "tournament_wiv");
    }

    @Override
    @Transactional(readOnly = true)
    public <T extends BaseDTO, SC extends BaseSC> List<T> get(Class<T> dtoClass, SC sc) {
        String canonicalName = dtoClass.getCanonicalName();
        String viewName = viewsNames.get(canonicalName);
        PropertyRowMapper<T> rowMapper = propertyRowMappers.get(canonicalName);

        QueryBuilder builder = new QueryBuilder("select * from " + viewName);
        if (sc != null) sc.fill(builder);
        return jdbcTemplate.query(builder.getQuery(), rowMapper, builder.getArguments());
    }

    @Override
    @Transactional(readOnly = true)
    public <T extends BaseDTO, SC extends BaseSC> long getCount(Class<T> dtoClass, SC sc) {
        String canonicalName = dtoClass.getCanonicalName();
        String viewName = viewsNames.get(canonicalName);

        QueryBuilder builder = new QueryBuilder("select count(*) from " + viewName);
        if (sc != null) sc.fill(builder);
        return jdbcTemplate.queryForObject(builder.getQuery(), Long.class, builder.getArguments());
    }

    @Override
    @Transactional(readOnly = true)
    public <T extends BaseDTO> T getById(Class<T> dtoClass, long id) {
        String canonicalName = dtoClass.getCanonicalName();
        String viewName = viewsNames.get(canonicalName);
        PropertyRowMapper<T> rowMapper = propertyRowMappers.get(canonicalName);

        QueryBuilder builder = new QueryBuilder("select * from " + viewName);
        builder.eq("id", id);
        return jdbcTemplate.queryForObject(builder.getQuery(), rowMapper, builder.getArguments());
    }

    @Override
    @Transactional(readOnly = false)
    public <T extends BaseDTO> Long post(T object) {
        PropertyPoster<T> propertyPoster = propertyPosters.get(object.getClass().getCanonicalName());
        return propertyPoster.post(object, jdbcTemplate);
    }

    @Override
    public <T extends BaseDTO> void delete(T object) {
        deleteById(object.getClass(), object.getId());
    }

    @Override
    @Transactional(readOnly = false)
    public <T extends BaseDTO> void deleteById(Class<T> dtoClass, long id) {
        String tableName = NamingUtil.tableNameFromDTOClassName(dtoClass.getSimpleName());
        jdbcTemplate.update("delete from " + tableName + " where id = ?", id);
    }
}
