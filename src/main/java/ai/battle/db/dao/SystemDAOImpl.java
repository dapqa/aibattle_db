package ai.battle.db.dao;

import ai.battle.db.dto.system.TournamentParticipantAccessDTO;
import ai.battle.db.sc.BaseSC;
import ai.battle.db.util.QueryBuilder;
import ai.battle.db.util.property.PropertyRowMapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by dapqa on 09.03.2016.
 */
@Service("systemDAO")
@SuppressWarnings("unchecked")
public class SystemDAOImpl extends CommonDAOImpl implements SystemDAO {

    @Override
    public List<TournamentParticipantAccessDTO> getOnlyAcceptedTournamentParticipantAccesses(BaseSC sc) {
        String canonicalName = TournamentParticipantAccessDTO.class.getCanonicalName();
        PropertyRowMapper<TournamentParticipantAccessDTO> tpaRowMapper = propertyRowMappers.get(canonicalName);

        QueryBuilder builder = new QueryBuilder("select * from tournament_participant_access_av");
        if (sc != null) sc.fill(builder);
        return jdbcTemplate.query(builder.getQuery(), tpaRowMapper, builder.getArguments());
    }
}
