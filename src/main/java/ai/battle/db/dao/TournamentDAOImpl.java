package ai.battle.db.dao;

import ai.battle.db.dto.tournament.TournamentDTO;
import ai.battle.db.sc.BaseSC;
import ai.battle.db.util.QueryBuilder;
import ai.battle.db.util.property.PropertyRowMapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by dapqa on 09.03.2016.
 */
@Service("tournamentDAO")
public class TournamentDAOImpl extends CommonDAOImpl implements TournamentDAO {

    @Override
    public List<TournamentDTO> getTournamentsFromTournamentView(BaseSC sc) {
        String canonicalName = TournamentDTO.class.getCanonicalName();
        PropertyRowMapper<TournamentDTO> tournamentRowMapper = propertyRowMappers.get(canonicalName);

        QueryBuilder builder = new QueryBuilder("select * from tournament_tv");
        if (sc != null) sc.fill(builder);
        return jdbcTemplate.query(builder.getQuery(), tournamentRowMapper, builder.getArguments());
    }

}
