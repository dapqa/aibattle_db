package ai.battle.db.util.property;

import ai.battle.db.dto.base.BaseDTO;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.JdbcUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Dapqa on 20.03.2016.
 */
public class PropertyRowMapper<T> implements RowMapper<T> {

    private final Class<T> mappedClass;
    private final String prefix;

    private class Column {
        public MappingProperty property;
        public int index;

        public Column(MappingProperty property, int index) {
            this.property = property;
            this.index = index;
        }
    }

    private class CompoundProperty {
        public MappingProperty property;
        public PropertyRowMapper rowMapper;

        public CompoundProperty(MappingProperty property) {
            this.property = property;

            rowMapper = new PropertyRowMapper<>(property.clazz, prefix + property.underlineName + "_");
        }
    }

    private HashMap<String, String> hints = new HashMap<>();
    private HashMap<String, MappingProperty> dtoProperties;
    private List<CompoundProperty> compoundProperties;

    private List<Column> currentColumns = new ArrayList<>();

    public PropertyRowMapper(Class<T> mappedClass) {
        this(mappedClass, "");
    }

    public PropertyRowMapper(Class<T> mappedClass, String prefix) {
        this.mappedClass = mappedClass;
        this.prefix = prefix;

        fillPossibleColumnsAndCompoundProperties();
    }

    @Override
    public T mapRow(ResultSet resultSet, int i) throws SQLException {
        if (i == 0) {
            fillCurrentColumns(resultSet);
        }

        try {
            T res = mappedClass.newInstance();

            for (Column column : currentColumns) {
                Object value = JdbcUtils.getResultSetValue(resultSet, column.index, column.property.clazz);
                column.property.setValue(res, value);
            }

            for (CompoundProperty compoundProperty : compoundProperties) {
                Object value = compoundProperty.rowMapper.mapRow(resultSet, i);
                compoundProperty.property.setValue(res, value);
            }

            return res;
        } catch (Exception e) {
            // TODO Лог
            return null;
        }
    }

    public PropertyRowMapper<T> addHint(String columnName, String requiredColumnOrPropertyName) {
        String underline = NamingUtil.toUnderline(requiredColumnOrPropertyName);

        hints.put(columnName, underline);
        for (CompoundProperty compoundProperty : compoundProperties) {
            compoundProperty.rowMapper.addHint(columnName, underline);
        }

        return this;
    }

    private void fillPossibleColumnsAndCompoundProperties() {
        List<Field> fields = new ArrayList<>();
        List<Method> methods = new ArrayList<>();
        DTOProperty.fillDeclaredFieldsAndPublicMethods(mappedClass, fields, methods);

        dtoProperties = new HashMap<>();
        compoundProperties = new ArrayList<>();
        boolean compound;
        for (Field field : fields) {
            compound = BaseDTO.class.isAssignableFrom(field.getType());

            String fieldName = field.getName();
            String setterName = NamingUtil.generateSetterName(fieldName);
            for (Method method : methods) {
                String methodName = method.getName();
                if (methodName.equals(setterName)) {
                    String underlineName = prefix + NamingUtil.toUnderline(fieldName);
                    MappingProperty mappingProperty = new MappingProperty(fieldName, underlineName, field.getType(), method);

                    if (!compound) {
                        dtoProperties.put(underlineName, mappingProperty);
                    } else {
                        compoundProperties.add(new CompoundProperty(mappingProperty));
                    }

                    break;
                }
            }
        }
    }

    private void fillCurrentColumns(ResultSet rs) throws SQLException {
        currentColumns.clear();

        ResultSetMetaData metaData = rs.getMetaData();
        int columnCount = metaData.getColumnCount();
        for (int i = 1; i <= columnCount; i++) {
            String columnName = metaData.getColumnName(i);
            if (hints.containsKey(columnName)) {
                columnName = hints.get(columnName);
            }

            // TODO Что делать с is_ в серередине?
            if (columnName.startsWith("is_")) {
                columnName = columnName.substring(3, columnName.length());
            }

            if ("state_name".equals(columnName)) {
                columnName = "state";
            }

            if (dtoProperties.containsKey(columnName)) {
                currentColumns.add(new Column(
                        dtoProperties.get(columnName), i
                ));
            }
        }
    }



}
