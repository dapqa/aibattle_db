package ai.battle.db.util.property;

/**
 * Created by dapqa on 20.03.2016.
 */
public class NamingUtil {

    public static String generateSetterName(String fieldName) {
        char[] chars = fieldName.toCharArray();
        chars[0] = Character.toUpperCase(chars[0]);
        return "set" + new String(chars);
    }

    public static String generateGetterName(String fieldName, boolean useIs) {
        char[] chars = fieldName.toCharArray();
        chars[0] = Character.toUpperCase(chars[0]);
        return (useIs ? "is" : "get") + new String(chars);
    }

    public static String toUnderline(String s) {
        StringBuilder stringBuilder = new StringBuilder();
        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            char ch = chars[i];
            if (Character.isLetter(ch) && Character.isUpperCase(ch)) {
                if (i != 0) stringBuilder.append('_');
                stringBuilder.append(Character.toLowerCase(ch));
            } else if (ch == '.') {
                stringBuilder.append('_');
            } else {
                stringBuilder.append(ch);
            }
        }
        return stringBuilder.toString();
    }

    public static String tableNameFromDTOClassName(String dtoClassName) {
        if (dtoClassName.endsWith("DTO")) dtoClassName = dtoClassName.substring(0, dtoClassName.length() - 3);
        return toUnderline(dtoClassName);
    }
}
