package ai.battle.db.util.property;

import ai.battle.db.dto.base.BaseDTO;
import ai.battle.db.util.AIBattleJdbcTemplate;
import org.omg.CORBA.ObjectHelper;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dapqa on 20.03.2016.
 */
public class PropertyPoster<T extends BaseDTO> {

    private final String tableName;

    private final String insertString;
    private final String updateString;

    private List<PostingProperty> postingProperties;

    public PropertyPoster(Class<T> postingClass, String tableName) {
        this.tableName = tableName;

        postingProperties = fillPostingProperties(postingClass, "");
        insertString = generateInsert();
        updateString = generateUpdate();
    }

    public PropertyPoster(Class<T> postingClass) {
        this(postingClass, NamingUtil.tableNameFromDTOClassName(postingClass.getSimpleName()));
    }

    public Long post(T obj, AIBattleJdbcTemplate jdbcTemplate) {
        try {
            if (obj.getId() == null) {
                Object[] arguments = generateArguments(obj, false);
                return jdbcTemplate.insertReturnId(insertString, arguments);
            } else {
                Object[] arguments = generateArguments(obj, true);
                jdbcTemplate.update(updateString, arguments);
                return obj.getId();
            }
        } catch (IllegalAccessException | InvocationTargetException e) {
            // TODO Лог
            return null;
        }
    }

    private List<PostingProperty> fillPostingProperties(Class clazz, String prefix) {
        List<Field> fields = new ArrayList<>();
        List<Method> methods = new ArrayList<>();
        DTOProperty.fillDeclaredFieldsAndPublicMethods(clazz, fields, methods);

        List<PostingProperty> res = new ArrayList<>();
        boolean compound;
        for (Field field : fields) {
            Class fieldType = field.getType();
            compound = BaseDTO.class.isAssignableFrom(fieldType);

            String fieldName = field.getName();
            String getterName1 = NamingUtil.generateGetterName(fieldName, false);
            String getterName2 = NamingUtil.generateGetterName(fieldName, true);
            for (Method method : methods) {
                String methodName = method.getName();
                if (methodName.equals(getterName1) || methodName.equals(getterName2)) {
                    String underlineName = prefix + NamingUtil.toUnderline(fieldName);

                    if ((fieldType == Boolean.class || fieldType == Boolean.TYPE) && prefix.isEmpty()) {
                        underlineName = "is_" + underlineName;
                    } else if ("state".equals(underlineName)) {
                        underlineName = "state_name";
                    }

                    res.add(new PostingProperty(fieldName, underlineName, fieldType, method, compound));

                    break;
                }
            }
        }

        return res;
    }

    private String generateInsert() {
        StringBuilder res = new StringBuilder();
        res.append("insert into ");
        res.append(tableName);
        res.append(" (");

        for (int i = 0; i < postingProperties.size(); i++) {
            PostingProperty cur = postingProperties.get(i);
            String columnName = cur.underlineName;

            if (!columnName.equals("id")) {
                if (cur.isCompound()) {
                    columnName += "_id";
                }
                res.append(columnName);

                res.append(',');
            }
        }
        res.deleteCharAt(res.length() - 1);

        res.append(") values (");
        for (int i = 0; i < postingProperties.size() - 1; i++) {
            res.append('?');
            if (i + 2 < postingProperties.size()) res.append(',');
        }
        res.append(')');

        return res.toString();
    }

    private String generateUpdate() {
        StringBuilder res = new StringBuilder();
        res.append("update ");
        res.append(tableName);
        res.append(" set ");

        for (int i = 0; i < postingProperties.size(); i++) {
            PostingProperty cur = postingProperties.get(i);
            String columnName = cur.underlineName;

            if (!columnName.equals("id")) {
                res.append(cur.underlineName);
                if (cur.isCompound()) {
                    res.append("_id");
                }
                res.append("=?,");
            }
        }
        res.deleteCharAt(res.length() - 1);
        res.append(" where id=?");

        return res.toString();
    }

    private Object[] generateArguments(T obj, boolean forUpdate)
            throws InvocationTargetException, IllegalAccessException
    {
        Object[] res = new Object[postingProperties.size() - (forUpdate ? 0 : 1)];

        Object id = null;
        int pos = 0;
        for (PostingProperty postingProperty : postingProperties) {
            Object property = postingProperty.getValue(obj);
            if (postingProperty.underlineName.equals("id")) {
                id = property;
            } else {
                if (postingProperty.isCompound()) {
                    if (property != null && property instanceof BaseDTO) {
                        res[pos] = ((BaseDTO)property).getId();
                    } else res[pos] = null;
                } else {
                    res[pos] = property;
                }
                pos++;
            }
        }

        if (forUpdate) res[res.length - 1] = id;
        return res;
    }

}
