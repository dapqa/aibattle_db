package ai.battle.db.util.property;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by dapqa on 20.03.2016.
 */
class PostingProperty extends DTOProperty {

    private boolean compound;

    private Method getter;
    private Method toString;

    public PostingProperty(
            String camelName, String underlineName, Class<?> clazz, Method getter,
            boolean compound
    ) {
        super(camelName, underlineName, clazz);
        this.getter = getter;
        this.compound = compound;

        if (isEnum) {
            try {
                toString = clazz.getMethod("toString");
            } catch (NoSuchMethodException e) {
                toString = null;
            }
        }
    }

    public boolean isCompound() {
        return compound;
    }

    public Object getValue(Object o) throws InvocationTargetException, IllegalAccessException {
        Object value = getter.invoke(o);
        if (isEnum) {
            if (toString == null) throw new NoSuchMethodError("Тип поля - Enum, но метод toString не был получен");
            value = toString.invoke(value);
        }

        return value;
    }

}
