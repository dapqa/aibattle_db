package ai.battle.db.util.property;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by dapqa on 20.03.2016.
 */
class DTOProperty {

    public String camelName;
    public String underlineName;
    public Class<?> clazz;

    protected boolean isEnum;
    protected boolean isDate;

    public DTOProperty(String camelName, String underlineName, Class<?> clazz) {
        this.camelName = camelName;
        this.underlineName = underlineName;
        this.clazz = clazz;

        isEnum = clazz.isEnum();
        isDate = clazz.equals(Date.class);
    }

    static void fillDeclaredFieldsAndPublicMethods(Class clazz, List<Field> fields, List<Method> methods) {
        do {
            Collections.addAll(fields, clazz.getDeclaredFields());
            Collections.addAll(methods, clazz.getMethods());
            clazz = clazz.getSuperclass();
        } while (clazz != Object.class);
    }

}
