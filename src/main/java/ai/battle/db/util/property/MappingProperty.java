package ai.battle.db.util.property;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Date;

/**
 * Created by dapqa on 20.03.2016.
 */
class MappingProperty extends DTOProperty {

    private Method setter;
    private Method valueOf;

    private static Method toInstant;

    static {
        try {
            toInstant = Timestamp.class.getMethod("toInstant");
        } catch (NoSuchMethodException e) {
            toInstant = null;
        }
    }

    public MappingProperty(String camelName, String underlineName, Class<?> clazz, Method setter) {
        super(camelName, underlineName, clazz);

        this.setter = setter;

        if (isEnum) {
            try {
                valueOf = clazz.getMethod("valueOf", String.class);
            } catch (NoSuchMethodException e) {
                valueOf = null;
            }
        }
    }

    public void setValue(Object o, Object value) throws InvocationTargetException, IllegalAccessException {
        if (isEnum) {
            if (valueOf == null) throw new NoSuchMethodError("Тип поля - Enum, но метод valueOf не был получен");
            value = valueOf.invoke(clazz, value);
        } else if (isDate) {
            if (toInstant == null) throw new NoSuchMethodError("Тип поля - Date, но метод toInstant не был получен");
            value = Date.from((Instant)toInstant.invoke(value));
        }
        setter.invoke(o, value);
    }

}
