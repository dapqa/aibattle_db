package ai.battle.db.util;

import org.springframework.jdbc.core.ArgumentPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import javax.sql.DataSource;
import java.sql.PreparedStatement;

/**
 * Created by dapqa on 19.03.2016.
 */
public class AIBattleJdbcTemplate extends JdbcTemplate {

    public AIBattleJdbcTemplate(DataSource dataSource) {
        super(dataSource);
    }

    public Long insertReturnId(String sql, Object... args) {
        PreparedStatementSetter preparedStatementSetter = new ArgumentPreparedStatementSetter(args);
        KeyHolder keyHolder = new GeneratedKeyHolder();

        update(connection -> {
            PreparedStatement ps = connection.prepareStatement(sql, new String[]{ "id" });
            preparedStatementSetter.setValues(ps);
            return ps;
        }, keyHolder);

        return keyHolder.getKey().longValue();
    }

}
