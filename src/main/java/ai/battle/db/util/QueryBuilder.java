package ai.battle.db.util;

import ai.battle.db.sc.SortingDescription;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by dapqa on 11.02.2016.
 */
public class QueryBuilder {

    private StringBuilder queryStringBuilder;
    private List<Object> arguments = new ArrayList<>();
    private boolean hasArguments = false;

    private Long limit;
    private Long offset;
    private List<SortingDescription> sortingDescriptionList = new ArrayList<>();

    public QueryBuilder(String query) {
        this.queryStringBuilder = new StringBuilder(query);
    }

    public String getQuery() {
        if (sortingDescriptionList.isEmpty() && limit == null && offset == null) {
            return queryStringBuilder.toString();
        } else {
            StringBuilder resBuilder = new StringBuilder(queryStringBuilder.toString());
            resBuilder.append(" order by ");

            for (SortingDescription sortingDescription : sortingDescriptionList) {
                resBuilder.append(sortingDescription.field);
                resBuilder.append(' ');
                resBuilder.append(sortingDescription.sorting.toString());
                resBuilder.append(',');
            }
            if (!sortingDescriptionList.isEmpty()) resBuilder.deleteCharAt(resBuilder.length() - 1);

            if (limit != null) {
                resBuilder.append(" limit ");
                resBuilder.append(limit);
            }

            if (offset != null) {
                resBuilder.append(" offset ");
                resBuilder.append(offset);
            }

            return resBuilder.toString();
        }

    }

    public Object[] getArguments() {
        return arguments.toArray(new Object[arguments.size()]);
    }

    private void addCondition(String condition, Object argument) {
        if (hasArguments) {
            queryStringBuilder.append(" and ");
        } else {
            queryStringBuilder.append(" where ");
            hasArguments = true;
        }

        queryStringBuilder.append(condition);
        if (argument != null) arguments.add(argument);
    }

    public QueryBuilder eq(String field, Object arg) {
        return operator(field, arg, "=");
    }

    public QueryBuilder neq(String field, Object arg) {
        return operator(field, arg, "!=");
    }

    public QueryBuilder g(String field, Object arg) {
        return operator(field, arg, ">");
    }

    public QueryBuilder ge(String field, Object arg) {
        return operator(field, arg, ">=");
    }

    public QueryBuilder l(String field, Object arg) {
        return operator(field, arg, "<");
    }

    public QueryBuilder le(String field, Object arg) {
        return operator(field, arg, "<=");
    }

    public QueryBuilder like(String field, String arg) {
        return operator(field, "%" + arg + "%", "like");
    }

    public QueryBuilder ilike(String field, String arg) {
        if (arg != null) {
            addCondition(String.format("lower(%s) like lower(?)", field), "%" + arg + "%");
        }
        return this;
    }

    public QueryBuilder isNull(String field) {
        addCondition(String.format("%s is null", field), null);
        return this;
    }

    public QueryBuilder isNotNull(String field) {
        addCondition(String.format("%s is not null", field), null);
        return this;
    }

    public QueryBuilder setLimit(Long limit) {
        this.limit = limit;
        return this;
    }

    public QueryBuilder setOffset(Long offset) {
        this.offset = offset;
        return this;
    }

    public QueryBuilder addSorting(String field, SortingDescription.Sorting sorting) {
        if (field != null && sorting != null) {
            sortingDescriptionList.add(new SortingDescription(field, sorting));
        }
        return this;
    }

    public QueryBuilder addSorting(String field) {
        return addSorting(field, SortingDescription.Sorting.ASC);
    }

    public QueryBuilder addSorting(List<SortingDescription> sortingDescriptionList) {
        this.sortingDescriptionList.addAll(
            sortingDescriptionList.stream()
                    .filter(sd -> sd.field != null)
                    .peek(sd -> { if (sd.sorting == null) sd.sorting = SortingDescription.Sorting.ASC; })
                    .collect(Collectors.toList())
        );
        return this;
    }

    private QueryBuilder operator(String field, Object arg, String operator) {
        if (arg != null) {
            addCondition(String.format("%s %s ?", field, operator), arg);
        }
        return this;
    }

}

