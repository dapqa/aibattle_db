package ai.battle.db.dto.tournament;

import ai.battle.db.dto.base.BaseNamedTournamentDTO;

/**
 * Created by dapqa on 09.03.2016.
 */
public class TournamentParameterDTO extends BaseNamedTournamentDTO {

    private String valueStr;
    private String description;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        TournamentParameterDTO that = (TournamentParameterDTO) o;

        if (valueStr != null ? !valueStr.equals(that.valueStr) : that.valueStr != null) return false;
        return !(description != null ? !description.equals(that.description) : that.description != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (valueStr != null ? valueStr.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    public String getValueStr() {
        return valueStr;
    }

    public void setValueStr(String valueStr) {
        this.valueStr = valueStr;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
