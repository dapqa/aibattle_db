package ai.battle.db.dto.tournament;

import ai.battle.db.dto.base.BaseDTO;

/**
 * Created by Dapqa on 14.03.2016.
 */
public class BotTournamentResultDTO extends BaseDTO {

    private TournamentRunDTO tournamentRun;
    private BotDTO bot;
    private Integer score;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        BotTournamentResultDTO that = (BotTournamentResultDTO) o;

        if (tournamentRun != null ? !tournamentRun.equalsById(that.tournamentRun) : that.tournamentRun != null)
            return false;
        if (bot != null ? !bot.equalsById(that.bot) : that.bot != null) return false;
        return !(score != null ? !score.equals(that.score) : that.score != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (tournamentRun != null ? tournamentRun.hashCode() : 0);
        result = 31 * result + (bot != null ? bot.hashCode() : 0);
        result = 31 * result + (score != null ? score.hashCode() : 0);
        return result;
    }

    public TournamentRunDTO getTournamentRun() {
        return tournamentRun;
    }

    public void setTournamentRun(TournamentRunDTO tournamentRun) {
        this.tournamentRun = tournamentRun;
    }

    public BotDTO getBot() {
        return bot;
    }

    public void setBot(BotDTO bot) {
        this.bot = bot;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }
}
