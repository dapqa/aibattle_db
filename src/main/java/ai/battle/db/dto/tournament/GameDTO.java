package ai.battle.db.dto.tournament;

import ai.battle.db.dto.base.BaseDTO;

/**
 * Created by dapqa on 09.03.2016.
 */
public class GameDTO extends BaseDTO {

    private TournamentRunDTO tournamentRun;
    private Boolean finished;
    private String logName;
    private String additionalInfo;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        GameDTO gameDTO = (GameDTO) o;

        if (tournamentRun != null ? !tournamentRun.equalsById(gameDTO.tournamentRun) : gameDTO.tournamentRun != null)
            return false;
        if (finished != null ? !finished.equals(gameDTO.finished) : gameDTO.finished != null) return false;
        if (logName != null ? !logName.equals(gameDTO.logName) : gameDTO.logName != null) return false;
        return !(additionalInfo != null ? !additionalInfo.equals(gameDTO.additionalInfo) : gameDTO.additionalInfo != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (tournamentRun != null ? tournamentRun.hashCode() : 0);
        result = 31 * result + (finished != null ? finished.hashCode() : 0);
        result = 31 * result + (logName != null ? logName.hashCode() : 0);
        result = 31 * result + (additionalInfo != null ? additionalInfo.hashCode() : 0);
        return result;
    }

    public TournamentRunDTO getTournamentRun() {
        return tournamentRun;
    }

    public void setTournamentRun(TournamentRunDTO tournamentRun) {
        this.tournamentRun = tournamentRun;
    }

    public Boolean isFinished() {
        return finished;
    }

    public void setFinished(Boolean finished) {
        this.finished = finished;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public String getLogName() {
        return logName;
    }

    public void setLogName(String logName) {
        this.logName = logName;
    }
}
