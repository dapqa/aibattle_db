package ai.battle.db.dto.tournament;

import ai.battle.db.dto.base.BaseNamedTournamentDTO;

/**
 * Created by dapqa on 09.03.2016.
 * TODO ����� ������ ������� � ��������� ����� ���������� ��-������-���-����?
 */
public class VisualizerDTO extends BaseNamedTournamentDTO {

    private String storagePath;
    private String description;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        VisualizerDTO that = (VisualizerDTO) o;

        if (storagePath != null ? !storagePath.equals(that.storagePath) : that.storagePath != null) return false;
        return !(description != null ? !description.equals(that.description) : that.description != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (storagePath != null ? storagePath.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    public String getStoragePath() {
        return storagePath;
    }

    public void setStoragePath(String storagePath) {
        this.storagePath = storagePath;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
