package ai.battle.db.dto.tournament;

import ai.battle.db.dto.base.BaseTournamentDTO;
import ai.battle.db.dto.system.AccountDTO;

import java.util.Date;

/**
 * Created by dapqa on 09.03.2016.
 */
public class TournamentRunDTO extends BaseTournamentDTO {

    private Date runDate;
    private Boolean finished;
    private AccountDTO admin;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        TournamentRunDTO that = (TournamentRunDTO) o;

        if (runDate != null ? !runDate.equals(that.runDate) : that.runDate != null) return false;
        if (finished != null ? !finished.equals(that.finished) : that.finished != null) return false;
        return !(admin != null ? !admin.equals(that.admin) : that.admin != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (runDate != null ? runDate.hashCode() : 0);
        result = 31 * result + (finished != null ? finished.hashCode() : 0);
        result = 31 * result + (admin != null ? admin.hashCode() : 0);
        return result;
    }

    public Date getRunDate() {
        return runDate;
    }

    public void setRunDate(Date runDate) {
        this.runDate = runDate;
    }

    public Boolean isFinished() {
        return finished;
    }

    public void setFinished(Boolean finished) {
        this.finished = finished;
    }

    public AccountDTO getAdmin() {
        return admin;
    }

    public void setAdmin(AccountDTO admin) {
        this.admin = admin;
    }
}
