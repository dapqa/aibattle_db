package ai.battle.db.dto.tournament;

import ai.battle.db.dto.base.BaseNamedDTO;
import ai.battle.db.dto.system.AccountDTO;

/**
 * Created by dapqa on 09.03.2016.
 */
public class TournamentDTO extends BaseNamedDTO {

    private String descriptionFilePath;
    private Boolean closed;
    private Boolean adminPrivate;
    private Boolean hidden;
    private AccountDTO creator;
    private LoggerDTO logger;
    private OrganizerDTO organizer;
    private TournamentSchemeDTO tournamentScheme;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        TournamentDTO that = (TournamentDTO) o;

        if (descriptionFilePath != null ? !descriptionFilePath.equals(that.descriptionFilePath) : that.descriptionFilePath != null)
            return false;
        if (closed != null ? !closed.equals(that.closed) : that.closed != null) return false;
        if (adminPrivate != null ? !adminPrivate.equals(that.adminPrivate) : that.adminPrivate != null) return false;
        if (hidden != null ? !hidden.equals(that.hidden) : that.hidden != null) return false;
        if (creator != null ? !creator.equalsById(that.creator) : that.creator != null) return false;
        if (logger != null ? !logger.equalsById(that.logger) : that.logger != null) return false;
        if (organizer != null ? !organizer.equalsById(that.organizer) : that.organizer != null) return false;
        return !(tournamentScheme != null ? !tournamentScheme.equalsById(that.tournamentScheme) : that.tournamentScheme != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (descriptionFilePath != null ? descriptionFilePath.hashCode() : 0);
        result = 31 * result + (closed != null ? closed.hashCode() : 0);
        result = 31 * result + (adminPrivate != null ? adminPrivate.hashCode() : 0);
        result = 31 * result + (hidden != null ? hidden.hashCode() : 0);
        result = 31 * result + (creator != null ? creator.hashCode() : 0);
        result = 31 * result + (logger != null ? logger.hashCode() : 0);
        result = 31 * result + (organizer != null ? organizer.hashCode() : 0);
        result = 31 * result + (tournamentScheme != null ? tournamentScheme.hashCode() : 0);
        return result;
    }

    public String getDescriptionFilePath() {
        return descriptionFilePath;
    }

    public void setDescriptionFilePath(String descriptionFilePah) {
        this.descriptionFilePath = descriptionFilePah;
    }

    public Boolean isClosed() {
        return closed;
    }

    public void setClosed(Boolean closed) {
        this.closed = closed;
    }

    public Boolean isAdminPrivate() {
        return adminPrivate;
    }

    public void setAdminPrivate(Boolean adminPrivate) {
        this.adminPrivate = adminPrivate;
    }

    public Boolean isHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public AccountDTO getCreator() {
        return creator;
    }

    public void setCreator(AccountDTO creator) {
        this.creator = creator;
    }

    public LoggerDTO getLogger() {
        return logger;
    }

    public void setLogger(LoggerDTO logger) {
        this.logger = logger;
    }

    public OrganizerDTO getOrganizer() {
        return organizer;
    }

    public void setOrganizer(OrganizerDTO organizer) {
        this.organizer = organizer;
    }

    public TournamentSchemeDTO getTournamentScheme() {
        return tournamentScheme;
    }

    public void setTournamentScheme(TournamentSchemeDTO tournamentScheme) {
        this.tournamentScheme = tournamentScheme;
    }
}
