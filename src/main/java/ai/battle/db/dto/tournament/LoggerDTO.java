package ai.battle.db.dto.tournament;

import ai.battle.db.dto.base.NamedRunnableDTO;

/**
 * Created by dapqa on 09.03.2016.
 */
public class LoggerDTO extends NamedRunnableDTO {

    public enum State {
        SENT, COMPILATION_OK, COMPILATION_ERR, INTERFACE_ERR, TEST_FAIL, ACCEPTED
    }

    private State state;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        LoggerDTO loggerDTO = (LoggerDTO) o;

        return state == loggerDTO.state;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (state != null ? state.hashCode() : 0);
        return result;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }
}
