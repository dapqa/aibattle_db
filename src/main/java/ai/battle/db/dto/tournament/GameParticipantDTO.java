package ai.battle.db.dto.tournament;

import ai.battle.db.dto.base.BaseDTO;

/**
 * Created by dapqa on 09.03.2016.
 */
public class GameParticipantDTO extends BaseDTO {

    private GameDTO game;
    private BotDTO bot;
    private Integer score;

    public GameDTO getGame() {
        return game;
    }

    public void setGame(GameDTO game) {
        this.game = game;
    }

    public BotDTO getBot() {
        return bot;
    }

    public void setBot(BotDTO bot) {
        this.bot = bot;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }
}
