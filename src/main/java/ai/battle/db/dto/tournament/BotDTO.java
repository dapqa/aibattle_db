package ai.battle.db.dto.tournament;

import ai.battle.db.dto.base.BaseTournamentDTO;
import ai.battle.db.dto.system.AccountDTO;

/**
 * Created by dapqa on 09.03.2016.
 */
public class BotDTO extends BaseTournamentDTO {

    public enum State {
        SENT, COMPILATION_OK, COMPILATION_ERR, INTERFACE_OK, INTERFACE_ERR, TEST_GAME_ERR, ACCEPTED, REPLACED
    }

    private String storagePath;
    private State state;
    private Boolean test;
    private AccountDTO author;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        BotDTO botDTO = (BotDTO) o;

        if (state != botDTO.state) return false;
        return !(author != null ? !author.equalsById(botDTO.author) : botDTO.author != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (state != null ? state.hashCode() : 0);
        result = 31 * result + (author != null ? author.hashCode() : 0);
        return result;
    }

    public Boolean isTest() {
        return test;
    }

    public void setTest(Boolean test) {
        this.test = test;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public AccountDTO getAuthor() {
        return author;
    }

    public void setAuthor(AccountDTO author) {
        this.author = author;
    }

    public String getStoragePath() {
        return storagePath;
    }

    public void setStoragePath(String storagePath) {
        this.storagePath = storagePath;
    }
}
