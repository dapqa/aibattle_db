package ai.battle.db.dto.tournament;

import ai.battle.db.dto.base.NamedRunnableDTO;

/**
 * Created by dapqa on 09.03.2016.
 */
public class OrganizerDTO extends NamedRunnableDTO {

    public enum State {
        SENT, COMPILATION_OK, COMPILATION_ERR, INTERFACE_OK, INTERFACE_ERR,
        TEST_GAME_ERR, ACCEPTED, REPLACED
    }

    private Integer playerCount;
    private State state;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        OrganizerDTO that = (OrganizerDTO) o;

        if (playerCount != null ? !playerCount.equals(that.playerCount) : that.playerCount != null) return false;
        return state == that.state;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (playerCount != null ? playerCount.hashCode() : 0);
        result = 31 * result + (state != null ? state.hashCode() : 0);
        return result;
    }

    public Integer getPlayerCount() {
        return playerCount;
    }

    public void setPlayerCount(Integer playerCount) {
        this.playerCount = playerCount;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

}
