package ai.battle.db.dto.system;

import ai.battle.db.dto.base.BaseDTO;

import java.util.Date;

/**
 * Created by dapqa on 09.03.2016.
 */
public class SystemLogDTO extends BaseDTO {

    public enum RecordType {
        INFO, ERROR
    }

    public enum RecordSection {
        SYSTEM, TOURNAMENT_SCHEME, ORGANIZER, LOGGER, BOT, TOURNAMENT
    }

    private Date recordDate;
    private String recordText;
    private AccountDTO account;
    private RecordType recordType;
    private RecordSection recordSection;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        SystemLogDTO that = (SystemLogDTO) o;

        if (recordDate != null ? !recordDate.equals(that.recordDate) : that.recordDate != null) return false;
        if (recordText != null ? !recordText.equals(that.recordText) : that.recordText != null) return false;
        if (account != null ? !account.equalsById(that.account) : that.account != null) return false;
        if (recordType != that.recordType) return false;
        return recordSection == that.recordSection;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (recordDate != null ? recordDate.hashCode() : 0);
        result = 31 * result + (recordText != null ? recordText.hashCode() : 0);
        result = 31 * result + (account != null ? account.hashCode() : 0);
        result = 31 * result + (recordType != null ? recordType.hashCode() : 0);
        result = 31 * result + (recordSection != null ? recordSection.hashCode() : 0);
        return result;
    }

    public Date getRecordDate() {
        return recordDate;
    }

    public void setRecordDate(Date recordDate) {
        this.recordDate = recordDate;
    }

    public String getRecordText() {
        return recordText;
    }

    public void setRecordText(String recordText) {
        this.recordText = recordText;
    }

    public AccountDTO getAccount() {
        return account;
    }

    public void setAccount(AccountDTO account) {
        this.account = account;
    }

    public RecordType getRecordType() {
        return recordType;
    }

    public void setRecordType(RecordType recordType) {
        this.recordType = recordType;
    }

    public RecordSection getRecordSection() {
        return recordSection;
    }

    public void setRecordSection(RecordSection recordSection) {
        this.recordSection = recordSection;
    }
}
