package ai.battle.db.dto.system;

import ai.battle.db.dto.base.NamedRunnableDTO;

/**
 * Created by dapqa on 09.03.2016.
 */
public class CompilerDTO extends NamedRunnableDTO {

    private String callString;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        CompilerDTO that = (CompilerDTO) o;

        return !(callString != null ? !callString.equals(that.callString) : that.callString != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (callString != null ? callString.hashCode() : 0);
        return result;
    }

    public String getCallString() {
        return callString;
    }

    public void setCallString(String callString) {
        this.callString = callString;
    }
}
