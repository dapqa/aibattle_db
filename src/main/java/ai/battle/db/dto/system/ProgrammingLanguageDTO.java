package ai.battle.db.dto.system;

import ai.battle.db.dto.base.BaseNamedDTO;

/**
 * Created by dapqa on 09.03.2016.
 */
public class ProgrammingLanguageDTO extends BaseNamedDTO {

    public enum CodeType {
        BINARY, BYTE_CODE, SCRIPT
    }

    private CodeType codeType;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        ProgrammingLanguageDTO that = (ProgrammingLanguageDTO) o;

        return codeType == that.codeType;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (codeType != null ? codeType.hashCode() : 0);
        return result;
    }

    public CodeType getCodeType() {
        return codeType;
    }

    public void setCodeType(CodeType codeType) {
        this.codeType = codeType;
    }
}
