package ai.battle.db.dto.system;

import ai.battle.db.dto.base.BaseTournamentAccountDTO;

/**
 * Created by dapqa on 09.03.2016.
 */
public class TournamentParticipantAccessDTO extends BaseTournamentAccountDTO {

    public enum State {
        IN_PROCESS, ACCEPTED, REJECTED
    }

    private State state;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        TournamentParticipantAccessDTO that = (TournamentParticipantAccessDTO) o;

        return state == that.state;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (state != null ? state.hashCode() : 0);
        return result;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

}
