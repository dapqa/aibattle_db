package ai.battle.db.dto.base;

import ai.battle.db.dto.system.AccountDTO;

/**
 * Created by dapqa on 09.03.2016.
 */
public class BaseTournamentAccountDTO extends BaseTournamentDTO {

    private AccountDTO account;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        BaseTournamentAccountDTO that = (BaseTournamentAccountDTO) o;

        return !(account != null ? !account.equalsById(that.account) : that.account != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (account != null ? account.hashCode() : 0);
        return result;
    }

    public AccountDTO getAccount() {
        return account;
    }

    public void setAccount(AccountDTO account) {
        this.account = account;
    }
}
