package ai.battle.db.dto.base;

import ai.battle.db.dto.tournament.TournamentDTO;

/**
 * Created by dapqa on 09.03.2016.
 */
public class BaseTournamentDTO extends BaseDTO {

    private TournamentDTO tournament;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        BaseTournamentDTO that = (BaseTournamentDTO) o;

        return !(tournament != null ? !tournament.equalsById(that.tournament) : that.tournament != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (tournament != null ? tournament.hashCode() : 0);
        return result;
    }

    public TournamentDTO getTournament() {
        return tournament;
    }

    public void setTournament(TournamentDTO tournament) {
        this.tournament = tournament;
    }
}
