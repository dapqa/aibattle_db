package ai.battle.db.dto.base;

/**
 * Created by dapqa on 09.03.2016.
 */
public class BaseDTO {

    private Long id;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BaseDTO baseDTO = (BaseDTO) o;

        return !(id != null ? !id.equals(baseDTO.id) : baseDTO.id != null);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    public boolean equalsById(BaseDTO other) {
        if (other == null) return false;
        if (id == null) return other.getId() == null;
        return id.equals(other.getId());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}

