package ai.battle.db.dto.base;

import ai.battle.db.dto.system.ProgrammingLanguageDTO;

/**
 * Created by dapqa on 09.03.2016
 * TODO Вероятно стоит разбить
 */
public class NamedRunnableDTO extends BaseNamedDTO {

    private String storagePath;
    private ProgrammingLanguageDTO programmingLanguage;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        NamedRunnableDTO that = (NamedRunnableDTO) o;

        if (storagePath != null ? !storagePath.equals(that.storagePath) : that.storagePath != null) return false;
        return !(programmingLanguage != null ? !programmingLanguage.equalsById(that.programmingLanguage) : that.programmingLanguage != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (storagePath != null ? storagePath.hashCode() : 0);
        result = 31 * result + (programmingLanguage != null ? programmingLanguage.hashCode() : 0);
        return result;
    }

    public String getStoragePath() {
        return storagePath;
    }

    public void setStoragePath(String storagePath) {
        this.storagePath = storagePath;
    }

    public ProgrammingLanguageDTO getProgrammingLanguage() {
        return programmingLanguage;
    }

    public void setProgrammingLanguage(ProgrammingLanguageDTO programmingLanguage) {
        this.programmingLanguage = programmingLanguage;
    }
}
