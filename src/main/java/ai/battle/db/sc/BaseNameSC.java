package ai.battle.db.sc;

import ai.battle.db.util.QueryBuilder;

/**
 * Created by dapqa on 21.03.2016.
 */
public class BaseNameSC extends BaseSC {

    private String name;

    @Override
    public void fill(QueryBuilder queryBuilder) {
        super.fill(queryBuilder);
        queryBuilder.ilike("name", name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
