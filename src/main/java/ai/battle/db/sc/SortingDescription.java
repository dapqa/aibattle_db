package ai.battle.db.sc;

/**
 * Created by dapqa on 21.03.2016.
 */
public class SortingDescription {

    public enum Sorting {
        ASC, DESC
    }

    public String field;
    public Sorting sorting;

    public SortingDescription(String field, Sorting sorting) {
        this.field = field;
        this.sorting = sorting;
    }

}
