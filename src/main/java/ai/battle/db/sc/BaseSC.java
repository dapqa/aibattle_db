package ai.battle.db.sc;

import ai.battle.db.util.QueryBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dapqa on 09.03.2016.
 */
public class BaseSC {

    private Long id;
    private Long limit;
    private Long offset;

    private List<SortingDescription> sortingDescriptionList = new ArrayList<>();

    public void fill(QueryBuilder queryBuilder) {
        queryBuilder.eq("id", id)
            .setLimit(limit)
            .setOffset(offset)
            .addSorting(sortingDescriptionList);
    }

    // TODO Дублирование кода с QueryBuilder
    public void addSorting(String field, SortingDescription.Sorting sorting) {
        if (field != null && sorting != null) {
            sortingDescriptionList.add(new SortingDescription(field, sorting));
        }
    }

    public void addSorting(String field) {
        addSorting(field, SortingDescription.Sorting.ASC);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getLimit() {
        return limit;
    }

    public void setLimit(Long limit) {
        this.limit = limit;
    }

    public Long getOffset() {
        return offset;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }
}
